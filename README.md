Inside that directory, you can run several commands:

- Runs the end-to-end tests.

```
  npx playwright test
```
- Runs the tests only on Desktop Chrome.
```
npx playwright test --project=chromium
```
- Runs the tests in the specific file.
```
npx playwright test example.spec.js or npx playwright test groups.spec.js
```
- Runs the tests in debug mode.
```
npx playwright test --debug
```

- We suggest that you begin by typing:

```
    npx playwright test
```

- And check out the following files:

  - ./example.spec.js - Example end-to-end test
  - ./playwright.config.js - Playwright Test configuration

- Ask for help
````
    npx playwright test --help
````

- Run tests in headed browsers
````
    npx playwright test --headed
````    

- To open last HTML report run:
````
  npx playwright show-report
````

- Playwright Inspector: inspect element, explore element, check selector, debug, record script
````
    npx playwright open
````

- Trace Viewer:
````
    npx playwright show-trace trace.zip
    go to https://trace.playwright.dev/ and upload trace.zip
````

- Generate tests: record
````
    npx playwright codegen <url>
````

- Test runner: install extension Playwright Test

- Install faker to generate data test: https://github.com/faker-js/faker
````
    npm install @faker-js/faker --save-dev
````
- Debug: https://playwright.dev/docs/debug#browser-developer-tools
    1. Using page.pause() method
    2. https://playwright.dev/docs/debug#use-the-javascript-debug-terminal


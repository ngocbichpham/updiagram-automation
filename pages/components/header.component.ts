import { expect, Locator, Page } from '@playwright/test';

export class HeaderComponent {
    readonly page: Page;
    readonly languageFlag: Locator;
    readonly languageEN: Locator;
    readonly signInButton: Locator;

    constructor(page: Page) {
        this.page = page;
        this.languageFlag = page.locator('.dropdown-flag');
        this.languageEN = page.locator('.dropdown-menu span.flag.flag-en');
        this.signInButton = page.locator('text=Sign In');
    }

    async selectLanguageEN() {
        // Click language dropdown
        await this.languageFlag.click();
        // Select EN
        await this.languageEN.click();
    }

    async clickSignIn() {
        await this.signInButton.click();
        await expect(this.page).toHaveURL(/.*login/);
    }
}
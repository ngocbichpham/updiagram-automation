import { expect, Locator, Page } from '@playwright/test';

export class LoginPage {
  readonly page: Page;
  readonly emailTextbox: Locator;
  readonly passwordTextbox: Locator;
  readonly signInButton: Locator;
  readonly tocList: Locator;

  constructor(page: Page) {
    this.page = page;
    this.emailTextbox = page.locator('[placeholder="Email"]');
    this.passwordTextbox = page.locator('[placeholder="Password"]');
    this.signInButton = page.locator('button', { hasText: 'Sign In' });
    this.tocList = page.locator('article div.markdown ul > li > a');
  }

  async enterEmail(email: string) {
    await this.emailTextbox.fill("");
    await this.emailTextbox.fill(email);
  }

  async enterPassword(password: string) {
    await this.passwordTextbox.fill(password);
  }

  async clickSignIn() {
    await this.signInButton.click();
  }
}
import { test, expect, } from '@playwright/test';
import { HeaderComponent } from '../../pages/components/header.component';
import { LoginPage } from '../../pages/login.page';

import fs from 'fs';
import path from 'path';
import { parse } from 'csv-parse/sync';

let loginPage: LoginPage;
let headerComponent: HeaderComponent;

const accounts = parse(fs.readFileSync(path.join(__dirname, '../../data/accounts.csv')), {
  columns: true,
  skip_empty_lines: true
});

const invalidEmails = parse(fs.readFileSync(path.join(__dirname, '../../data/invalid-emails.csv')), {
  columns: true,
  skip_empty_lines: true
});

test.describe('Login', () => {

  test.beforeEach(async ({ page }) => {
    loginPage = new LoginPage(page);
    headerComponent = new HeaderComponent(page);

    // Go to http://testv1.updiagram.cf/
    await page.goto('/');

    // Select EN language
    await headerComponent.selectLanguageEN();
  });

  for (const record of accounts) {
    test(`Verify login successfuly with valid email ${record.email} and password`, async ({ page }) => {
      console.log(record.test_case, record.email, record.password);
      await clickSignIn(page);

      await enterEmail(record.email);

      await enterPassword(record.password);

      await clickSignInButton();

      await test.step('Verify user is on dashboard page', async () => {
        await expect(page).toHaveURL(/.*dashboard/);
      });
    });
  }

  test('Verify login unsuccessfuly with valid email and incorrect password', async ({ page }) => {
    await clickSignIn(page);

    await enterEmail(accounts[0].email);

    await enterPassword('Test@2021');

    await clickSignInButton();

    await test.step('Verify error is displayed', async () => {
      await expect(page.locator('.alert.alert-danger')).toContainText('Username or password is incorrect');
    });
  });

  test('Verify login unsuccessfuly with incorrect email and correct password', async ({ page }) => {
    await clickSignIn(page);

    await enterEmail(`1234567${accounts[0].email}`);

    await enterPassword(accounts[0].password);

    await clickSignInButton();

    await test.step('Verify error is displayed', async () => {
      await expect(page.locator('.alert.alert-danger')).toContainText('Username or password is incorrect');
    });
  });

  test('Verify that the system will show an alert message when user Sign in with correct Email and empty password', async ({ page }) => {
    await clickSignIn(page);

    await enterEmail(accounts[0].email);

    // Check password empty
    await expect(loginPage.passwordTextbox).toBeEmpty();

    await clickSignInButton();

    await test.step('Verify errors are displayed correctly', async () => {
      await expect(page.locator('input[name="password"] + .error')).toContainText('Password is required');
    });
  });

  test('Verify that the system will show an alert message when user Sign in with empty Email and correct password', async ({ page }) => {
    await clickSignIn(page);
    // Check email empty
    await expect(loginPage.emailTextbox).toBeEmpty();

    await enterPassword(accounts[0].password);

    await clickSignInButton();

    await test.step('Verify errors are displayed correctly', async () => {
      await expect(page.locator('input[name="email"] + .error')).toContainText('Email is required');
    });
  });

  test('Verify that system shows error when user leaves email and password empty and click Sign In', async ({ page }) => {
    await clickSignIn(page);

    // Check email empty
    await expect(loginPage.emailTextbox).toBeEmpty();

    // Check password empty
    await expect(loginPage.passwordTextbox).toBeEmpty();

    await clickSignInButton();

    await test.step('Verify errors are displayed correctly', async () => {
      await expect(page.locator('input[name="email"] + .error')).toContainText('Email is required');
      await expect(page.locator('input[name="password"] + .error')).toContainText('Password is required');
    });
  });

  test('Verify that system shows error when user email with incorrect format', async ({ page }) => {
    await clickSignIn(page);
    for await (const record of invalidEmails) {
      console.log('Check invalid email:', record.invalidEmail)
      await enterEmail(record.invalidEmail);
      await clickSignInButton();
      await test.step('Verify errors are displayed correctly', async () => {
        await expect(page.locator('input[name="email"] + .error')).toContainText('The Email field is not a valid e-mail address');
        await expect(page.locator('input[name="password"] + .error')).toContainText('Password is required');
      });
    }
  });

  async function clickSignIn(page) {
    await test.step('Click Sign In Link', async () => {
      // Click text=Sign In
      await headerComponent.clickSignIn();
      await expect(page).toHaveURL(/.*login/);
    });
  };

  async function enterEmail(email: string) {
    await test.step('Enter email', async () => {
      // Fill [placeholder="Email"]
      await loginPage.enterEmail(email);
    });
  };

  async function enterPassword(password: string) {
    await test.step('Enter password', async () => {
      // Fill [placeholder="Password"
      await loginPage.enterPassword(password);
    });
  };

  async function clickSignInButton() {
    await test.step('Click Sign In button', async () => {
      // Click Sign In button 
      await loginPage.clickSignIn();
    });
  };
});